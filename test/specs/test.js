var webdriverio = require('webdriverio');


describe('foundry page', () => {
    it('should have the right title', () => {
        browser.url('http://foundry:30000')
        expect(browser).toHaveTitle('Foundry Virtual Tabletop • A Standalone Virtual Tabletop Application');
    })

    it('should should setup Kobold Kauldron', () => {
        const checkbox = $('#eula-agree')
        if (checkbox.error === undefined) {
            checkbox.click();

            const agree = $("#sign");
            agree.click();
        }

        const adminInput = $('#key');
        adminInput.setValue('atropos');

        const submitKey = $('[name=action]');
        submitKey.click();

        const gameSystemTab = $('[data-tab=systems]');
        gameSystemTab.click();
        browser.saveScreenshot('test.png');

        $('.tab.active button.install-package').click();

        browser.saveScreenshot('install.png');
        
        const packageFilter = $('[name=filter]');
        packageFilter.waitForDisplayed({timeout:3000});
        $('[name=filter]').setValue('DnD5e');
        browser.saveScreenshot('filter.png');

        const package = $('[data-package-id=dnd5e]');
        package.waitForDisplayed({timeout:3000});

        browser.saveScreenshot('system.png');
        package.$('button[data-manifest*=dnd5e]').click()
        browser.saveScreenshot('installed.png');

        const worldsTab = $('[data-tab=worlds]');
        worldsTab.click();
        $('.tab.active button.install-package').click();
        
        $('[data-manifest*=kobold-cauldron]').click();

        browser.pause(30000);
        browser.refresh();
        browser.saveScreenshot('pre.png');

        const launchWorld = $('[data-world=kobold-cauldron]');
        launchWorld.click();
        browser.pause(3000);
        browser.saveScreenshot('kobold.png');
    });
})