# WARNING:

This is a hamfisted attempt to prove out Selenium and FVTT.  You will find things that will upset you or not make sense, sorry!  This assumes a lot and does not attempt to be reproducable or usable. 

# Installation Requires
- npm
- docker-compose


## Setup
- Set your credentials in the `secrets.json`
- Run `docker-compose up`
- Run `npm install`
- Run `mkdir /tmp/fvtt` or change the data dir in the `docker-compose.yml`.
- Run `npx wdio wdio.conf.js`
- Run `rm -rf /tmp/fvtt/*` between tests, this assumes a clean setup every time.